#include "api/api.h"
#include "api/init.h"

#include <string.h>
#include <stdio.h>
#include <rtthread.h>

static void recv_sms(char *num , char *body)
{
	char *serial_out_buf = rt_malloc(256);
	ApiSendSms(num,body);
	if (serial_out_buf)
	{
		snprintf(serial_out_buf,256,"[SMS]%s,%s\n",num,body);
		ApiWriteSerial(USART_PORT_1,serial_out_buf,strlen(serial_out_buf));
		rt_free(serial_out_buf);
	}
}

static void recv_serial_data(USART_PORT_NUM port,unsigned char *buffer , unsigned int size)
{

	//接受串口数据，并作相应处理。
	if (size == 3)
	{
		if (strstr(buffer,"+++"))
		{
			unsigned char __tmp = 0xEE;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0xffffffff);
		}
		else if (strstr(buffer,"---"))
		{
			unsigned char __tmp = 0x00;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0x0);
		}
	}

}

void  StartupImpl(void)
{

}
void RecvSysEventImpl(EI_SYS_EVENT event , void *body)
{

	switch(event)
	{
		case RECVEVENT_SMS: //收到SMS信息
			recv_sms(
					((struct RcvSmsEvent*)body)->num,
					((struct RcvSmsEvent*)body)->body
				);
			break;
		case RECVEVENT_SERIAL: //串口有信息到来
			recv_serial_data(
					((struct RcvSerialDataEvent*)body)->port,
					((struct RcvSerialDataEvent*)body)->data,
					((struct RcvSerialDataEvent*)body)->len
					);
			break;
		default:
			break;
	}
	//
}

