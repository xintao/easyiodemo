#include "api/api.h"
#include "api/init.h"

static void recv_xmpp(char *from , char *msg)
{
	static struct GsmLocInfo info;
	ApiGetGsmLoc(&info);
	
	ApiSendXmppMsg(from,info.longitude);
	ApiSendXmppMsg(from,info.latitude);
}

static void recv_sms(char *num , char *body)
{
	ApiSendSms(num,body);
}

static void recv_tcp_data(unsigned char * buffer , unsigned int size)
{
	ApiWriteSerial(USART_PORT_1,buffer,size);
	ApiSendTcpData(buffer,size);
}

static void press_serial_data(unsigned char data , unsigned int len)
{
}

static void recv_serial_data(USART_PORT_NUM port,unsigned char *buffer , unsigned int size)
{
	//ApiWriteSerial(USART_PORT_2,buffer,size);
	if (size == 3)
	{
		if (strstr(buffer,"+++"))
		{
			char __tmp = 0xEE;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0xffffffff);
		}
		else if (strstr(buffer,"---"))
		{
			char __tmp = 0x00;
			ApiWriteEEPROM(0,&__tmp,1);
			ApiSetSysDebugMask(0x0);
		}
	}
	else
		ApiSendTcpData(buffer,size);
}

void  StartupImpl(void)
{
	unsigned char tmp;
	struct GprsNetworkOption gprs_opt;
	struct TcpcliOption tcp_opt;
	struct CloudOption cloud_opt;

	ApiReadEEPROM(0,&tmp,1);
	if (tmp == 0xEE)
		ApiSetSysDebugMask(0xffffffff);
	
	ApiEnableWatchDog(); //打开看门狗
  snprintf(gprs_opt.apn,sizeof(gprs_opt.apn),"%s","CMNET");
  InitGprsNetwork(&gprs_opt); //初始化GPRS网络

	snprintf(tcp_opt.host,sizeof(tcp_opt.host),"%s","liud-home.f3322.org");
	snprintf(tcp_opt.port,sizeof(tcp_opt.port),"%s","5000");
	snprintf(tcp_opt.heartbeat_buffer,sizeof(tcp_opt.heartbeat_buffer),"%s","easyio!!!!!!");
	tcp_opt.heartbeat_cycle = 60;
	tcp_opt.heartbeat_len = sizeof("easyio!!!!!!");
	InitTcpcli(&tcp_opt);

	cloud_opt.type = EASYIO_SERVICE;
	snprintf(cloud_opt.password,sizeof(cloud_opt.password),"111111");
	InitEasyIOCloud(&cloud_opt);

	//ApiSetSysDebugMask(0xffffffff);

}
void RecvSysEventImpl(EI_SYS_EVENT event , void *body)
{

	switch(event)
	{
		case RECVEVENT_SMS: //收到SMS信息
			recv_sms(
					((struct RcvSmsEvent*)body)->num,
					((struct RcvSmsEvent*)body)->body
				);
			break;
		case RECVEVENT_TCP: //收到TCP数据
			recv_tcp_data(
					((struct RcvTcpDataEvent*)body)->data,
					((struct RcvTcpDataEvent*)body)->len
				     );
			break;
		case RECVEVENT_SERIAL: //串口有信息到来
			recv_serial_data(
					((struct RcvSerialDataEvent*)body)->port,
					((struct RcvSerialDataEvent*)body)->data,
					((struct RcvSerialDataEvent*)body)->len
					);
			break;
		case RECVEVENT_XMPPMSG:
			recv_xmpp(
					((struct RcvXmppMsgEvent*)body)->from,
					((struct RcvXmppMsgEvent*)body)->body
				 );
			break;
		case SYSEVENT_TCP_CONNECTED:
			ApiSendTcpData("login dtu server .",sizeof("login dtu server ."));
			break;
		default:
			break;
	}
	//
}

