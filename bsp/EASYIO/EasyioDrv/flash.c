#include "stm32f10x_flash.h"
#include "dbug/dbug.h"
int write_flash(u32 StartAddr,u16 *buf,u16 len)
{
	volatile FLASH_Status FLASHStatus;
	u32 FlashAddr;
	len=(len+1)/2;
	FLASH_Unlock();
	FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
	FlashAddr=StartAddr;
	FLASH_ErasePage(StartAddr);
	while(len--){
		FLASHStatus = FLASH_ProgramHalfWord(FlashAddr,*buf++);
		if (FLASHStatus != FLASH_COMPLETE){
			np_dprintf(DEBUG_SYS,"FLSH :Error %08X\n\r",FLASHStatus);
			return -1;
		}
		FlashAddr += 2;
	}
	FLASH_Lock();
	return 0;

}
int read_flash(u32 StartAddr,u16 *buf,u16 len)
{
	u16 *p=(u16 *)StartAddr;
	len=(len+1)/2;
	while(len--){
		*buf++=*p++;
	}
	return 0;
}

